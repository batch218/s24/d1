// console.log("Hello World")

// ECMA5script is the standard that is used to create implementations of the language, one which is Javascript.
// where all browser vendor can implement(Apple, Google, Microsoft, Mozilla, etc.);

// New features to JavaScript

// [SECTION] Exponent Operator
console.log("ES6 Update");
console.log("===========================");
console.log("Exponent Operator");

// Using Math Object Methods
console.log("Using the Object Methods")
const firstNum = Math.pow(8,3); // 8*8*8 = 512
console.log(firstNum);

//Using the exponent operator
console.log("Using the exponent operator")
const secondNum = 8 ** 3; 
console.log(secondNum);

// [SECTION] Template Literals
/*
	- Allows to write string without using the concatenation operator (+).
	- ${} is called placeholder when using template literals, and we can input variables or expression.

*/

console.log("--------------------------");
console.log("Template Literals");
let name = "John";

console.log("Using concatination");
let message = " Hello "  + name + " Welcome to programming" ;
console.log(message);

console.log("Message with template literals");
message = `Hello ${name} Welcome to programming`;
console.log(message);

// [SECTION] Array Destructuring

// It allows us to name array elements with variableNames instead of using the index numbers.
/*
	- Syntax:
		let/const [variableName1, variableName2, variableName3] = arrayName;
*/

console.log(`=======================`);
console.log("Array Destructuring");

const fullName = ["Juan", "Dela", "Cruz"]
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! Its nice to meet you!`);
console.log(`=======================`);

// Array Destructuring 
// Variable naming for array destructuring is based on the developers choice

const [firstName, middleName, LastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(LastName);

console.log(`Hello ${firstName} ${middleName} ${LastName}! Its nice to meet you!`);

// [SECTION] Object Destructuring 
/*
  - Shortens the syntax for accessing properties form object
  - The difference with array destructuring, this should be exact propert name,
  -Syntax:
  let/const {propertyNameA,propertyNameB,propertyNameC } objects;

*/
console.log(`=======================`);
console.log("Object Destructuring");



const person = {
	givenName: "Jane",
	mName: "Dela",
	surName: "Cruz"
}

//Pre-object destructuring
console.log(person.givenName);
console.log(person.mName);
console.log(person.surName);

console.log(`Hello ${person.givenName} ${person.middleName} ${person.SurName}! It's good to see you again`);
console.log("-----");

// Object Desctructuring
const {givenName, mName, surName} = person;

console.log(givenName);
console.log(mName);
console.log(surName);

console.log(`Hello ${givenName} ${mName} ${surName}! It's good to see you again`);

console.log(`=======================`);
console.log("Arrow Function");

// [SECTION] Arrow Functions
/*
    - Compact alternative syntax to traditioal functions
    - Useful for code snippets where creating functions will not be reused in any other portion of code.
    - this will work with "function expression." (s17 )
    example of funcExpression
    let funcExpression = function funcName(){
      console.log("Hello");
    }
    funcExpresion();
*/

/*
Syntax
let/const variableName = (parameter) => {
  //code to execute
}
invocation
variableName(argument);
 */

const hello = () => {
  console.log("Hello from the other side");
}
hello();
console.log("using function with forEach")
const students = ["John", "Jane","Judy"];
students.forEach(function(student){
  console.log(`${student} is a student`)
})

/*
  Syntax:
  arrayName.arrayMethod(parameter) =>{
    // code to execute;
  }
*/
console.log(`=======================`);
console.log("using arrow with forEach")
students.forEach(students => {
  console.log(`${students} is a student`)
});


// anonymous function = a function that has no function name

console.log ("===========================");
console.log("Implicit Return using arror functions");
// [SECTION] Implicit Return Statement
// There are instances when you can omit the "return" statement.
// Implicit return means - Returns the statement/value even withour the return keyword;
// const add = (x, y) =>{
// 	return x + y;
// }

/*
const add = (x,y)=>{
  x+y;
}
*/
/*
Syntax:
  let/const variableName = (parameter/s) => code to execute;

*/
// arrow method
const add = (x,y) => x+y;

let total = add(1,2);
console.log(total);

/*
//function 
function add(x,y) {
  return
}

let total = add(1,2);
console.log(total);
*/

//[SECTION] Default Function Argument Value
// Provides a default argument value if none is provided when the function is invoked.

// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefined

const greet = (name = "User") => `Good morning ${name}`;
console.log(greet());
console.log(greet("John"));

console.log ("===========================");
console.log ("Class-Based Object Blueprint: ");
/* 
[SECTION] Class-Based Object Blueprint
Another approach in creating an object with key and value;
Allows creation/instantiation of object using classes as blueprints.
*/
// - The "constructor" is a special method of a class for creating/initializing object for that class.

/*
- Syntax:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
			// insert function outside our constructor
		}
*/
class Car{
  constructor(brand,name,year){
    this.brand = brand;
    this.name = name ;
    this.year = year ;
  }
}
// "new" 
const myCar = new Car() ;
console.log(myCar);

console.log ("===========================");
// Reassigning value of each property
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = "2021";
console.log(myCar);